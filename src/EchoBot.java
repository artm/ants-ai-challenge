import java.io.*;

public class EchoBot {
    private OutputStream output;
    {
        try {
            output = new FileOutputStream("debug.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void readInput(InputStream inputStream) throws IOException {
        StringBuilder line = new StringBuilder();
        int c;
        while ((c = inputStream.read()) >= 0) {
            if (c == '\r' || c == '\n') {
                processLine(line.toString().toLowerCase().trim());
                line.setLength(0);
            } else {
                line = line.append((char)c);
            }
        }
    }

    public void readInput(Reader reader) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            processLine(line);
        }
    }

    public static void main(String[] args) throws IOException {
        new EchoBot().readInput(new InputStreamReader(System.in));
    }

    public void processLine(String s) throws IOException {
        output.write(s.getBytes());
    }
}
