import com.ftech.ants.Ants;
import com.ftech.ants.AntsParser;
import com.ftech.ants.Bot;
import com.ftech.ants.decision.DecisionManager;
import com.ftech.ants.model.Aim;
import com.ftech.ants.model.Ant;
import com.ftech.ants.model.Movement;

import java.io.*;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Starter bot implementation.
 */
// https://bitbucket.org/artm/ants-ai-challenge/wiki/Useful%20links
public class MyBot implements Bot {
    public static final InputStream BOT_INPUT = System.in;
    public static final PrintStream BOT_OUTPUT = System.out;

    static {
        if (debug)
            try {
                AntsParser.debugInfo = new PrintWriter(new FileWriter("debug.txt", false), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    private AntsParser antsParser;
    private Ants ants;

    public MyBot(InputStream sourceStream) {
        antsParser = new AntsParser(sourceStream);
    }

    /**
	 * Main method executed by the game engine for starting the bot.
	 * 
	 * @param args command line arguments
	 * @throws IOException if an I/O error occurs
	 * @throws URISyntaxException
	 */
	public static void main(String[] args) throws IOException, URISyntaxException {
//		new MyBot().readInput(new FileInputStream("test-data/timeout.txt"));
//		new MyBot().readInput(Bot.class.getClassLoader().getResourceAsStream("6.txt"));
//		new MyBot().readInput(Bot.class.getClassLoader().getResourceAsStream("timeout5.txt"));
		 new MyBot(BOT_INPUT).go();
//		 new MyBot(new FileInputStream("out/production/debug.txt")).go();
	}

    public void go() throws IOException {
        Movement movement = antsParser.nextMovement();
        while (movement != null) {
            if (movement.getAction() == AntsParser.Action.READY) {
                ants = new Ants(movement);
            } else if (movement.getAction() == AntsParser.Action.GO) {
                ants.update(movement);
                doTurn();
            }

            finishTurn();

            movement = antsParser.nextMovement();
        }
    }

	@Override
	public void doTurn() {
		if (outputDebug) {
			Ants.debugInfo.println(ants.getTurnNo());
		}

		DecisionManager decisionManager = ants.getDecisionManager();
		decisionManager.makeDecision();
		for (Ant ant : ants.getMyAnts()) {
			Map.Entry<Aim, Double> maxProbabilityTurn = ant.getMaxProbabilityTurn(ants.getRandom());
			if (maxProbabilityTurn.getKey() != Aim.NONE)
				ants.issueOrder(ant.getCurrentPosition(), maxProbabilityTurn.getKey());
		}
	}

    @Override
    public void finishTurn() {
        BOT_OUTPUT.println(AntsParser.Action.GO.name().toLowerCase());
        BOT_OUTPUT.flush();
    }
}