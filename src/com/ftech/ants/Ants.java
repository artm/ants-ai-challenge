package com.ftech.ants;

import com.ftech.ants.decision.DecisionManager;
import com.ftech.ants.model.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Holds all game data and current game state.
 */
public class Ants {
    /** Maximum map size. */
    public static final int MAX_MAP_SIZE = 256;
    
    private final int loadTime;
    private final int turnTime;
    private final int rows;
    private final int cols;
    private final int turns;
    private final int viewRadius2;
    private final int attackRadius2;
    private final int spawnRadius2;

    private int turnNo;
    private long turnStartTime;

    private Random random;

    private final Ilk map[][];
    
    private final Map<Tile, Ant> myAnts = new LinkedHashMap<Tile, Ant>();
    private final Set<Tile> enemyAnts = new LinkedHashSet<Tile>();
    private final Set<Tile> myHills = new LinkedHashSet<Tile>();
    private final Set<Tile> visibleEnemyHills = new LinkedHashSet<Tile>();
    private final Set<Tile> invisibleEnemyHills = new LinkedHashSet<Tile>();
    private final Set<Tile> foodTiles = new LinkedHashSet<Tile>();
    private final Set<Order> orders = new LinkedHashSet<Order>();

    private DecisionManager decisionManager;

    public static PrintWriter debugInfo;
    static  {
        if (Bot.outputDebug)
            try {
                int number = 0;
                while (new File("out" + number + ".txt").exists())
                    number++;

                debugInfo = new PrintWriter(new FileWriter("out" + number + ".txt", false), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    /**
     * Creates new {@link Ants} object.
     *
     * @param movement Data object to extract params
     */
    public Ants(Movement movement) {
        this.loadTime = (Integer) movement.get(AntsParser.SetupToken.LOADTIME).get(0);
        this.turnTime = (Integer) movement.get(AntsParser.SetupToken.TURNTIME).get(0);
        this.rows = (Integer) movement.get(AntsParser.SetupToken.ROWS).get(0);
        this.cols = (Integer) movement.get(AntsParser.SetupToken.COLS).get(0);
        this.turns = (Integer) movement.get(AntsParser.SetupToken.TURNS).get(0);
        this.viewRadius2 = (Integer) movement.get(AntsParser.SetupToken.VIEWRADIUS2).get(0);
        this.attackRadius2 = (Integer) movement.get(AntsParser.SetupToken.ATTACKRADIUS2).get(0);
        this.spawnRadius2 = (Integer) movement.get(AntsParser.SetupToken.SPAWNRADIUS2).get(0);
        random = new Random((Long) movement.get(AntsParser.SetupToken.PLAYER_SEED).get(0));

        map = new Ilk[rows][cols];
        for (Ilk[] row : map) {
            Arrays.fill(row, Ilk.UNEXPLORED);
        }

        decisionManager = new DecisionManager(this);
    }
    
    /**
     * Returns timeout for initializing and setting up the bot on turn 0.
     * 
     * @return timeout for initializing and setting up the bot on turn 0
     */
    public int getLoadTime() {
        return loadTime;
    }
    
    /**
     * Returns timeout for a single game turn, starting with turn 1.
     * 
     * @return timeout for a single game turn, starting with turn 1
     */
    public int getTurnTime() {
        return turnTime;
    }
    
    /**
     * Returns game map height.
     * 
     * @return game map height
     */
    public int getRows() {
        return rows;
    }
    
    /**
     * Returns game map width.
     * 
     * @return game map width
     */
    public int getCols() {
        return cols;
    }
    
    /**
     * Returns maximum number of turns the game will be played.
     * 
     * @return maximum number of turns the game will be played
     */
    public int getTurns() {
        return turns;
    }
    
    /**
     * Returns squared view radius of each ant.
     * 
     * @return squared view radius of each ant
     */
    public int getViewRadius2() {
        return viewRadius2;
    }
    
    /**
     * Returns squared attack radius of each ant.
     * 
     * @return squared attack radius of each ant
     */
    public int getAttackRadius2() {
        return attackRadius2;
    }
    
    /**
     * Returns squared spawn radius of each ant.
     * 
     * @return squared spawn radius of each ant
     */
    public int getSpawnRadius2() {
        return spawnRadius2;
    }
    
    /**
     * Sets turn start time.
     * 
     * @param turnStartTime turn start time
     */
    public void setTurnStartTime(long turnStartTime) {
        this.turnStartTime = turnStartTime;
    }
    
    /**
     * Returns how much time the bot has still has to take its turn before timing out.
     * 
     * @return how much time the bot has still has to take its turn before timing out
     */
    public int getTimeRemaining() {
        return turnTime - (int)(System.currentTimeMillis() - turnStartTime);
    }
    
    /**
     * Returns ilk at the specified location.
     * 
     * @param tile location on the game map
     * @return ilk at the <cod>tile</code>
     */
    public Ilk getIlk(Tile tile) {
        return map[tile.getRow()][tile.getCol()];
    }
    
    /**
     * Sets ilk at the specified location.
     * 
     * @param tile location on the game map
     * @param ilk ilk to be set at <code>tile</code>
     */
    public void setIlk(Tile tile, Ilk ilk) {
        map[tile.getRow()][tile.getCol()] = ilk;
    }
    
    /**
     * Returns ilk at the location in the specified direction from the specified location.
     * 
     * @param tile location on the game map
     * @param direction direction to look up
     * 
     * @return ilk at the location in <code>direction</code> from <cod>tile</code>
     */
    public Ilk getIlk(Tile tile, Aim direction) {
        Tile newTile = getTile(tile, direction);
        return map[newTile.getRow()][newTile.getCol()];
    }
    
    /**
     * Returns location in the specified direction from the specified location.
     * 
     * @param tile location on the game map
     * @param direction direction to look up
     * 
     * @return location in <code>direction</code> from <cod>tile</code>
     */
    public Tile getTile(Tile tile, Aim direction) {
        return normalize(new Tile(tile.getRow() + direction.getRowDelta(), tile.getCol() + direction.getColDelta()));
    }
    
    /**
     * Returns a set containing all my ants locations.
     * 
     * @return a set containing all my ants locations
     */
    public Collection<Ant> getMyAnts() {
        return myAnts.values();
    }
    
    public Collection<Tile> getMyAntsPositions() {
    	List<Tile> positions = new ArrayList<Tile>(myAnts.values().size());
    	for (Ant ant : myAnts.values()) {
    		positions.add(ant.getCurrentPosition());
    	}
    	return positions;
    }
    
    /**
     * Returns a set containing all enemy ants locations.
     * 
     * @return a set containing all enemy ants locations
     */
    public Set<Tile> getEnemyAnts() {
        return enemyAnts;
    }
    
    /**
     * Returns a set containing all my hills locations.
     * 
     * @return a set containing all my hills locations
     */
    public Set<Tile> getMyHills() {
        return myHills;
    }
    
    /**
     * Returns a set containing all enemy hills locations.
     * 
     * @return a set containing all enemy hills locations
     */
    public Set<Tile> getVisibleEnemyHills() {
        return visibleEnemyHills;
    }

    public Set<Tile> getInvisibleEnemyHills() {
        return invisibleEnemyHills;
    }

    /**
     * Returns a set containing all food locations.
     * 
     * @return a set containing all food locations
     */
    public Set<Tile> getFoodTiles() {
        return foodTiles;
    }
    
    /**
     * Returns all orders sent so far.
     * 
     * @return all orders sent so far
     */
    public Set<Order> getOrders() {
        return orders;
    }
    
    /**
     * Calculates distance between two locations on the game map.
     * 
     * @param t1 one location on the game map
     * @param t2 another location on the game map
     * 
     * @return squared distance between <code>t1</code> and <code>t2</code>
     */
    public int getDistance(Tile t1, Tile t2) {
        int rowDelta = Math.abs(t1.getRow() - t2.getRow());
        int colDelta = Math.abs(t1.getCol() - t2.getCol());
        rowDelta = Math.min(rowDelta, rows - rowDelta);
        colDelta = Math.min(colDelta, cols - colDelta);
        return rowDelta * rowDelta + colDelta * colDelta;
    }
    
    /**
     * Returns one or two orthogonal directions from one location to the another.
     * 
     * @param t1 one location on the game map
     * @param t2 another location on the game map
     * 
     * @return orthogonal directions from <code>t1</code> to <code>t2</code>
     */
    public List<Aim> getDirections(Tile t1, Tile t2) {
        List<Aim> directions = new ArrayList<Aim>();
        if (t1.getRow() < t2.getRow()) {
            if (t2.getRow() - t1.getRow() >= rows / 2) {
                directions.add(Aim.NORTH);
            } else {
                directions.add(Aim.SOUTH);
            }
        } else if (t1.getRow() > t2.getRow()) {
            if (t1.getRow() - t2.getRow() >= rows / 2) {
                directions.add(Aim.SOUTH);
            } else {
                directions.add(Aim.NORTH);
            }
        }
        if (t1.getCol() < t2.getCol()) {
            if (t2.getCol() - t1.getCol() >= cols / 2) {
                directions.add(Aim.WEST);
            } else {
                directions.add(Aim.EAST);
            }
        } else if (t1.getCol() > t2.getCol()) {
            if (t1.getCol() - t2.getCol() >= cols / 2) {
                directions.add(Aim.EAST);
            } else {
                directions.add(Aim.WEST);
            }
        }
        return directions;
    }
    
    /**
     * Clears game state information about my ants locations.
     */
    public void clearMyAnts() {
        for (Ant myAnt : myAnts.values()) {
            map[myAnt.getCurrentPosition().getRow()][myAnt.getCurrentPosition().getCol()] = Ilk.LAND;
        }
        myAnts.clear();
    }
    
    /**
     * Clears game state information about enemy ants locations.
     */
    public void clearEnemyAnts() {
        for (Tile enemyAnt : enemyAnts) {
            map[enemyAnt.getRow()][enemyAnt.getCol()] = Ilk.LAND;
        }
        enemyAnts.clear();
    }
    
    /**
     * Clears game state information about my hills locations.
     */
    public void clearMyHills() {
        myHills.clear();
    }
    
    /**
     * Clears game state information about enemy hills locations.
     */
    public void clearEnemyHills() {
        visibleEnemyHills.clear();
    }

    /**
     * Enables performing actions which should take place prior to updating the game state, like
     * clearing old game data.
     */
    private void beforeUpdate() {
        turnStartTime = System.currentTimeMillis();
        clearMyAnts();
        clearEnemyAnts();
        clearMyHills();
        clearEnemyHills();
        foodTiles.clear();
        orders.clear();
    }

    public void update(Movement movement) {
        beforeUpdate();

        for (Object o : movement.get(AntsParser.UpdateToken.W)) {
            int[] water = (int[]) o;
            update(Ilk.WATER, new Tile(water[0], water[1]));
        }

        for (Object o : movement.get(AntsParser.UpdateToken.A)) {
            int[] ants = (int[]) o;
            update(ants[2] > 0 ? Ilk.ENEMY_ANT : Ilk.MY_ANT, new Tile(ants[0], ants[1]));
        }

        for (Object o : movement.get(AntsParser.UpdateToken.F)) {
            int[] foods = (int[]) o;
            update(Ilk.FOOD, new Tile(foods[0], foods[1]));
        }

        for (Object o : movement.get(AntsParser.UpdateToken.D)) {
            int[] dead = (int[]) o;
            update(dead[2] > 0 ? Ilk.ENEMY_DEAD : Ilk.MY_DEAD, new Tile(dead[0], dead[1]));
        }

        for (Object o : movement.get(AntsParser.UpdateToken.H)) {
            int[] hills = (int[]) o;
            updateHills(hills[2], new Tile(hills[0], hills[1]));
        }

        setTurnNo((Integer) movement.get(AntsParser.UpdateToken.TURN).get(0));

        explore();
    }
    
    /**
     * Updates game state information about new ants and food locations.
     * 
     * @param ilk ilk to be updated
     * @param tile location on the game map to be updated
     */
    public void update(Ilk ilk, Tile tile) {
        map[tile.getRow()][tile.getCol()] = ilk;
        switch (ilk) {
            case FOOD:
                foodTiles.add(tile);
            break;
            case MY_ANT:
                myAnts.put(tile, new Ant(tile));
            break;
            case ENEMY_ANT:
                enemyAnts.add(tile);
            break;
        }
    }
    
    /**
     * Updates game state information about hills locations.
     *
     * @param owner owner of hill
     * @param tile location on the game map to be updated
     */
    public void updateHills(int owner, Tile tile) {
        if (owner > 0) {
            visibleEnemyHills.add(tile);
            invisibleEnemyHills.add(tile);
        } else
            myHills.add(tile);
    }
    
    /**
     * Issues an order by sending it to the system output.
     * 
     * @param myAnt map tile with my ant
     * @param direction direction in which to move my ant
     */
    public void issueOrder(Tile myAnt, Aim direction) {
        Order order = new Order(myAnt, direction);
        orders.add(order);
        System.out.println(order);
        System.out.flush();

        if (Bot.outputDebug)
            debugInfo.println(order);
    }

    public void explore() {
        int r = (int) Math.sqrt(viewRadius2);
        for (Ant ant : myAnts.values()) {
            for (int row = ant.getCurrentPosition().getRow() - r ; row <= ant.getCurrentPosition().getRow() + r ; row++ )
                for (int col = ant.getCurrentPosition().getCol() - r ; col <= ant.getCurrentPosition().getCol() + r ; col++ ) {
                    Tile tile = normalize(new Tile(row, col));

                    if (getDistance(ant.getCurrentPosition(), tile) <= viewRadius2
                            && map[tile.getRow()][tile.getCol()] == Ilk.UNEXPLORED) {
                        map[tile.getRow()][tile.getCol()] = Ilk.LAND;
                    }

                    if (getDistance(ant.getCurrentPosition(), tile) <= viewRadius2 && invisibleEnemyHills.contains(tile) &&
                            !visibleEnemyHills.contains(tile)) {
                        // remove destroyed enemy hill
                        invisibleEnemyHills.remove(tile);
                    }
                }
        }
    }

    public Tile normalize(Tile tile) {
        int col = tile.getCol(), row = tile.getRow();

        if (col >= cols) col -= cols;
        if (col < 0) col += cols;

        if (row >= rows) row -= rows;
        if (row < 0) row += rows;

        return new Tile(row, col);
    }

    public DecisionManager getDecisionManager() {
        return decisionManager;
    }

    public Ant getAnt(Tile tile) {
        return myAnts.get(tile);
    }

    public Random getRandom() {
        return random;
    }

    public void setTurnNo(int turnNo) {
        this.turnNo = turnNo;
    }

    public int getTurnNo() {
        return turnNo;
    }
}
