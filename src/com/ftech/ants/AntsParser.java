package com.ftech.ants;

import com.ftech.ants.model.Movement;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class AntsParser {
    private static final char COMMENT_CHAR = '#';

    private BufferedReader source;

    // todo debug
    public static PrintWriter debugInfo;

    public enum Action {
        GO, READY
    }

    public enum SetupToken {
        LOADTIME, TURNTIME, ROWS, COLS, TURNS, VIEWRADIUS2, ATTACKRADIUS2, SPAWNRADIUS2, PLAYER_SEED;

        private static final Pattern PATTERN = compilePattern(SetupToken.class);
    }

    public enum UpdateToken {
        TURN, W, A, F, D, H;

        private static final Pattern PATTERN = compilePattern(UpdateToken.class);
    }

    private static Pattern compilePattern(Class<? extends Enum> clazz) {
        StringBuilder builder = new StringBuilder("(");
        for (Enum enumConstant : clazz.getEnumConstants()) {
            if (enumConstant.ordinal() > 0) {
                builder.append("|");
            }
            builder.append(enumConstant.name());
        }
        builder.append(")");
        return Pattern.compile(builder.toString());
    }

    public AntsParser(InputStream sourceStream) {
        source = new BufferedReader(new InputStreamReader(sourceStream));
//        source = sourceStream;
    }

    /**
     * Parses the setup information from system input stream.
     *
     * @param input setup information
     * @param movement object to populate info
     */
    public void parseSetup(List<String> input, Movement movement) {
        for (String line : input) {
            line = removeComment(line);
            if (line.isEmpty()) {
                continue;
            }
            Scanner scanner = new Scanner(line);
            if (!scanner.hasNext()) {
                continue;
            }
            String token = scanner.next().toUpperCase();
            if (!SetupToken.PATTERN.matcher(token).matches()) {
                continue;
            }
            SetupToken setupToken = SetupToken.valueOf(token);
            switch (setupToken) {
                case PLAYER_SEED:
                    movement.put(SetupToken.PLAYER_SEED, scanner.nextLong());
                break;
                default:
                    movement.put(setupToken, scanner.nextInt());
                break;
            }
        }
    }

    /**
     * Parses the update information from system input stream.
     *
     * @param input update information
     * @param movement object to populate info
     */
    public void parseUpdate(List<String> input, Movement movement) {
        for (String line : input) {
            line = removeComment(line);
            if (line.isEmpty()) {
                continue;
            }
            Scanner scanner = new Scanner(line);
            if (!scanner.hasNext()) {
                continue;
            }
            String token = scanner.next().toUpperCase();
            if (!UpdateToken.PATTERN.matcher(token).matches()) {
                continue;
            }
            UpdateToken updateToken = UpdateToken.valueOf(token);
            int row = 0, col = 0;

            if (updateToken != UpdateToken.TURN) {
                row = scanner.nextInt();
                col = scanner.nextInt();
            }

            Object info = null;
            switch (updateToken) {
                case TURN:
                    if (scanner.hasNextInt()) {
                        info = scanner.nextInt();
                    }
                break;
                case W:
                    info = new int[] {row, col};
                break;
                case A:
                    if (scanner.hasNextInt()) {
                        info = new int[] {row, col, scanner.nextInt()};
                    }
                break;
                case F:
                    info = new int[] {row, col};
                break;
                case D:
                    if (scanner.hasNextInt()) {
                        info = new int[] {row, col, scanner.nextInt()};
                    }
                break;
                case H:
                    if (scanner.hasNextInt()) {
                        info = new int[] {row, col, scanner.nextInt()};
                    }
                break;
            }

            movement.put(updateToken, info);
        }
    }

//    StringBuilder line = new StringBuilder();

    private String readLine() throws IOException {
//        int c;
//        while ((c = source.read()) >= 0) {
//            if (c == '\r' || c == '\n') {
//                String result = line.toString().toLowerCase().trim();
//                line.setLength(0);
//                return result;
//            } else {
//                line = line.append((char)c);
//            }
//        }

        return source.readLine();
    }

    public Movement nextMovement() throws IOException {
        Movement movement = new Movement();

        List<String> input = new ArrayList<String>();

        String line;
        while ((line = readLine()) != null) {
            if (Bot.debug) {
                debugInfo.println(line);
            }

            if (Action.READY.name().toLowerCase().equals(line)) {
                movement.setAction(Action.READY);
                parseSetup(input, movement);
                return movement;
            } else if (Action.GO.name().toLowerCase().equals(line)) {
                movement.setAction(Action.GO);
                parseUpdate(input, movement);
                return movement;
            }

            input.add(line);
        }

        return null;
    }

    private String removeComment(String line) {
        int commentCharIndex = line.indexOf(COMMENT_CHAR);
        String lineWithoutComment;
        if (commentCharIndex >= 0) {
            lineWithoutComment = line.substring(0, commentCharIndex).trim();
        } else {
            lineWithoutComment = line;
        }
        return lineWithoutComment;
    }
}
