package com.ftech.ants;

import java.io.PrintWriter;

public interface Bot {
    public static final boolean debug = false;
    public static final boolean outputDebug = false;
    public static final boolean stepDebug = false;

    public void doTurn();
    public void finishTurn();
}
