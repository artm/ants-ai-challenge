package com.ftech.ants;

import java.util.Calendar;

import com.ftech.ants.evaluators.LocalConflict.LocalConflictEvaluator;
import com.ftech.ants.evaluators.LocalConflict.LocalConflictEvaluator.Situation;
import com.ftech.ants.model.Tile;

public class DebugUtils {

	public static void printMap(int[][] map, int[][] conflicts, Ants ants) {
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[i].length; j++) {
				String field = String.format("%3d", map[i][j]);
				if (ants.getAnt(new Tile(i, j)) != null) {
					field = "    X";
				} else if (ants.getEnemyAnts().contains(new Tile(i, j))) {
					field = "    Y";
				} else if (conflicts[i][j] == LocalConflictEvaluator.CONFLICT) {
					field = field + ".C";
				} else {
					field = "  " + field;
				}
				Ants.debugInfo.print(field);
			}
			Ants.debugInfo.println();
		}
		Ants.debugInfo.println();

	}

	public static void printEdgeCases(Situation situation, Ants ants) {
		if (situation.enemyAnts.size() == 1 && situation.myAnts.size() == 1 && Bot.outputDebug) {
			Ants.debugInfo
					.println("ALARM_1x1:" + ants.getDistance(situation.enemyAnts.get(0), situation.myAnts.get(0)));
		}
		if (situation.myAnts.size() == 1 && Bot.outputDebug) {
			Ants.debugInfo.println("ALARM_x1:" + situation.enemyAnts.size());
		}

	}

	private static long time = -1;

	public static void endTime(Object message) {
		System.out.println(message.toString() + " takes - " + (Calendar.getInstance().getTimeInMillis() - time));
	}

	public static void startTime() {
		time = Calendar.getInstance().getTimeInMillis();
	}
}
