package com.ftech.ants.decision;

import java.util.*;

import com.ftech.ants.Ants;
import com.ftech.ants.evaluators.*;
import com.ftech.ants.evaluators.LocalConflict.DebugUtils;
import com.ftech.ants.evaluators.LocalConflict.LocalConflictEvaluator;
import com.ftech.ants.model.Ant;
import com.ftech.ants.model.Probability;

public class DecisionManager {
	private List<Evaluator> evaluators = new ArrayList<Evaluator>();
	private List<HighOrderEvaluator> highOrderEvaluators = new ArrayList<HighOrderEvaluator>();

	public DecisionManager(Ants ants) {
		evaluators.add(new FoodFindEvaluator(ants));
		evaluators.add(new ExplorerEvaluator(ants));
		// evaluators.add(new DefenceEvaluator(ants));
		evaluators.add(new AggroEvaluator(ants));
		evaluators.add(new RazingHillEvaluator(ants));
		evaluators.add(new LocalConflictEvaluator(ants));

		highOrderEvaluators.add(new SelfKillProtector(ants));
	}

	@SuppressWarnings({"unchecked"})
    public void makeDecision() {
        Map<Ant, Probability> computedProbability = new LinkedHashMap<Ant, Probability>();
		for (Evaluator evaluator : evaluators) {
			DebugUtils.startTime();
			DebugUtils.printMessage(evaluator.getClass().getName() + " has been started");

            Map<Ant, Probability> preComputedProb = evaluator.evaluate();
            for (Ant ant : preComputedProb.keySet()) {
                Probability preComp = preComputedProb.get(ant);
                if (computedProbability.containsKey(ant)) {
                    computedProbability.get(ant).addProbability(preComp, evaluator.getWeight());
                } else {
                    computedProbability.put(ant, preComp);
                }
            }

            DebugUtils.endTime(evaluator.getClass().getName());
		}

        for (HighOrderEvaluator highOrderEvaluator : highOrderEvaluators) {
            computedProbability = highOrderEvaluator.evaluate(computedProbability);
        }

        for (Ant ant : computedProbability.keySet()) {
            ant.setAntProbability(computedProbability.get(ant));
        }
	}
}
