package com.ftech.ants.evaluators;

import com.ftech.ants.Ants;
import com.ftech.ants.model.*;

import java.util.*;

public class AggroEvaluator implements Evaluator {
    private static final double WEIGHT = 0.5;

    private Ants ants;

    public AggroEvaluator(Ants ants) {
        this.ants = ants;
    }

    @Override
    public Map<Ant, Probability> evaluate() {
        Map<Ant, Probability> evaluation = new LinkedHashMap<Ant, Probability>();

        Map<Ant, List<Way>> closestToTile = new LinkedHashMap<Ant, List<Way>>();
        for (Tile enemyHill : ants.getInvisibleEnemyHills()) {
            Map<Ant, Way> closestAnts = findClosestAnts(enemyHill);

            for (Ant ant : closestAnts.keySet()) {
                List<Way> wayList = closestToTile.get(ant);

                if (wayList == null) {
                    wayList = new ArrayList<Way>();
                    closestToTile.put(ant, wayList);
                }
                wayList.add(closestAnts.get(ant));
            }
        }

        for (Ant ant : closestToTile.keySet()) {
            // for each ant we choose minimum way to hill
            List<Way> wayList = closestToTile.get(ant);
            Way minWay = Collections.min(wayList, new Comparator<Way>() {
                @Override
                public int compare(Way o1, Way o2) {
                    return o1.getDistance() - o2.getDistance();
                }
            });

            Probability p = new Probability();
            for (Aim aim : minWay.getDirections()) {
                p.put(aim, 1.0 / minWay.getDirections().size());
            }

            evaluation.put(ant, p);
        }

        return evaluation;
    }

    @Override
    public double getWeight() {
        return WEIGHT;
    }

    private Map<Ant, Way> findClosestAnts(Tile startPoint) {
        int[][] nums = new int[ants.getRows()][ants.getCols()];
        for (int[] num : nums) {
            Arrays.fill(num, -1);
        }

        nums[startPoint.getRow()][startPoint.getCol()] = 0;
        Queue<Tile> ways = new ArrayDeque<Tile>();
        ways.add(startPoint);

        int s = 1;

        Map<Ant, Way> antWays = new LinkedHashMap<Ant, Way>();

        while (!ways.isEmpty()) {
            Queue<Tile> newWays = new ArrayDeque<Tile>();
            for (Tile way : ways) {
                for (Aim aim : Aim.directions()) {
                    Tile nextDestination = ants.getTile(way, aim);

                    if (nums[nextDestination.getRow()][nextDestination.getCol()] == -1) {
                        if (ants.getIlk(nextDestination) == Ilk.MY_ANT) {
                            Set<Aim> aims = new HashSet<Aim>();
                            for (Aim a : Aim.directions()) {
                                Tile tile = ants.getTile(nextDestination, a);
                                if (nums[tile.getRow()][tile.getCol()] == s - 1) {
                                    aims.add(a);
                                }
                            }
                            antWays.put(ants.getAnt(nextDestination), new Way(s, aims));
                        }

                        if (ants.getIlk(nextDestination).isPassable()) {
                            newWays.add(nextDestination);
                            nums[nextDestination.getRow()][nextDestination.getCol()] = s;
                        }
                    }
                }
            }

            s++;
            ways.clear();
            ways.addAll(newWays);
        }

        return antWays;
    }
}
