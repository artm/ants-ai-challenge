//package com.ftech.ants.evaluators;
//
//import com.ftech.ants.Ants;
//import com.ftech.ants.model.Aim;
//import com.ftech.ants.model.Ant;
//import com.ftech.ants.model.Ilk;
//import com.ftech.ants.model.Tile;
//
//import java.util.*;
//
//public class DefenceEvaluator implements Evaluator {
//    private static final double WEIGHT = 3;
//
//    private static final double DEFENDERS_PERCENT = 0.5;
//    private static final int DEFENDERS_MAX = 20;
//
//    private static final int DEFENCE_DISTANCE = 5;
//
//    private Ants ants;
//
//    public DefenceEvaluator(Ants ants) {
//        this.ants = ants;
//    }
//
//    @Override
//    public void evaluate() {
//        for (Tile hill : ants.getMyHills()) {
//            int defenders = Math.min(DEFENDERS_MAX, (int) (DEFENDERS_PERCENT * ants.getMyAnts().size()) / ants.getMyHills().size());
//
//            Map<Ant, Set<Aim>> closestAnts = findClosestAnts(hill, defenders);
//        }
//    }
//
//    private Map<Ant, Set<Aim>> findClosestAnts(Tile startPoint, int antCount) {
//        int[][] nums = new int[ants.getRows()][ants.getCols()];
//        for (int[] num : nums)
//            Arrays.fill(num, -1);
//
//        nums[startPoint.getRow()][startPoint.getCol()] = 0;
//        Queue<Tile> ways = new ArrayDeque<Tile>();
//        ways.add(startPoint);
//
//        Map<Ant, Set<Aim>> closestAnts = new LinkedHashMap<Ant, Set<Aim>>();
//
//        int s = 1;
//        while (!ways.isEmpty() && closestAnts.size() < antCount) {
//            Queue<Tile> newWays = new ArrayDeque<Tile>();
//            for (Tile way : ways) {
//                for (Aim aim : Aim.directions()) {
//                    Tile nextDestination = ants.getTile(way, aim);
//
//                    if (nums[nextDestination.getRow()][nextDestination.getCol()] == -1) {
//                        if (ants.getIlk(nextDestination) == Ilk.MY_ANT) {
//                            if (closestAnts.size() < antCount)
//                                closestAnts.put(ants.getAnt(nextDestination), new LinkedHashSet<Aim>());
//                        }
//
//                        if (ants.getIlk(nextDestination).isPassable()) {
//                            newWays.add(nextDestination);
//                            nums[nextDestination.getRow()][nextDestination.getCol()] = s;
//                        }
//                    }
//                }
//            }
//
//            s++;
//            ways.clear();
//            ways.addAll(newWays);
//        }
//
//        for (Ant ant : closestAnts.keySet()) {
//            for (Aim aim : Aim.directions()) {
//                Tile next = ants.getTile(ant.getCurrentPosition(), aim);
//                if (nums[next.getRow()][next.getCol()] + 1 == nums[ant.getCurrentPosition().getRow()][ant.getCurrentPosition().getCol()]) {
//                    closestAnts.get(ant).add(aim);
//                }
//            }
//        }
//
//        return closestAnts;
//    }
//}
