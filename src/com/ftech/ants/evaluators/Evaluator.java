package com.ftech.ants.evaluators;

import com.ftech.ants.model.Aim;
import com.ftech.ants.model.Ant;
import com.ftech.ants.model.Probability;

import java.util.Map;
import java.util.Set;

public interface Evaluator {
    public Map<Ant, Probability> evaluate();
    public double getWeight();
}
