package com.ftech.ants.evaluators;

import com.ftech.ants.Ants;
import com.ftech.ants.model.*;

import java.util.*;

public class ExplorerEvaluator implements Evaluator {
    public static final double WEIGHT = 0.5;
    private Ants ants;

    public ExplorerEvaluator(Ants ants) {
        this.ants = ants;
    }

    @Override
    public Map<Ant, Probability> evaluate() {
        Map<Ant, Probability> evaluation = new LinkedHashMap<Ant, Probability>();

        for (Tile hill : ants.getMyHills()) {
            Map<Ant, Map<Aim, Double>> closestAnts = bfs(hill);
            for (Ant ant : closestAnts.keySet()) {
                double sum = 0;
                for (Aim aim : closestAnts.get(ant).keySet())
                    sum += closestAnts.get(ant).get(aim);

                Probability probabilities = new Probability();
                for (Aim aim : closestAnts.get(ant).keySet()) {
                    probabilities.put(aim, (closestAnts.get(ant).get(aim) + 0.0) / sum);
                }

                if (evaluation.get(ant) == null) {
                    evaluation.put(ant, probabilities);
                } else {
                    evaluation.get(ant).addProbability(probabilities, 1);
                }
            }
        }

        return evaluation;
    }

    @Override
    public double getWeight() {
        return WEIGHT;
    }

    private Map<Ant, Map<Aim, Double>> bfs(Tile startPoint) {
        int[][] nums = new int[ants.getRows()][ants.getCols()];
        for (int[] num : nums)
            Arrays.fill(num, -1);

        nums[startPoint.getRow()][startPoint.getCol()] = 0;
        Queue<Tile> ways = new ArrayDeque<Tile>();
        ways.add(startPoint);

        Map<Ant, Map<Aim, Double>> closestAnts = new LinkedHashMap<Ant, Map<Aim, Double>>();
        Set<Tile> unexploredTiles = new LinkedHashSet<Tile>();

        int s = 1;
        while (!ways.isEmpty()) {
            Queue<Tile> newWays = new ArrayDeque<Tile>();
            for (Tile way : ways) {
                for (Aim aim : Aim.directions()) {
                    Tile nextDestination = ants.getTile(way, aim);

                    if (nums[nextDestination.getRow()][nextDestination.getCol()] == -1) {
                        if (ants.getIlk(nextDestination).isPassable()) {
                            newWays.add(nextDestination);
                            nums[nextDestination.getRow()][nextDestination.getCol()] = s;
                        } else if (ants.getIlk(nextDestination) == Ilk.UNEXPLORED) unexploredTiles.add(nextDestination);
                    }
                }
            }

            s++;
            ways.clear();
            ways.addAll(newWays);
        }

        for (Tile unexploredTile : unexploredTiles) {
            Set<Tile> steps = new LinkedHashSet<Tile>();
            int exploreDistance = -1;
            for (Aim aim : Aim.directions()) {
                Tile closestReachable = ants.getTile(unexploredTile, aim);
                if (nums[closestReachable.getRow()][closestReachable.getCol()] != -1) {
                    steps.add(closestReachable);
                    exploreDistance = nums[closestReachable.getRow()][closestReachable.getCol()];
                }
            }

            while (!steps.isEmpty()) {
                Set<Tile> newSteps = new LinkedHashSet<Tile>();
                for (Tile step : steps) {
                    for (Aim aim : Aim.directions()) {
                        Tile next = ants.getTile(step, aim);
                        if (nums[next.getRow()][next.getCol()] == nums[step.getRow()][step.getCol()] - 1) {
                            newSteps.add(next);
                            if (ants.getIlk(next) == Ilk.MY_ANT) {
                                Ant ant = ants.getAnt(next);

                                if (closestAnts.get(ant) == null)
                                    closestAnts.put(ant, new LinkedHashMap<Aim, Double>());

                                if (closestAnts.get(ant).get(aim.negate()) == null)
                                    closestAnts.get(ant).put(aim.negate(), 0.0);

                                closestAnts.get(ant).put(aim.negate(), closestAnts.get(ant).get(aim.negate()) +
                                                1.0 / (exploreDistance - nums[next.getRow()][next.getCol()]));
                            }
                        }
                    }
                }

                steps.clear();
                steps.addAll(newSteps);
            }
        }

        return closestAnts;
    }
}
