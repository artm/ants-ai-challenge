package com.ftech.ants.evaluators;

import com.ftech.ants.Ants;
import com.ftech.ants.model.*;

import java.util.*;

public class FoodFindEvaluator implements Evaluator {
    private Ants ants;

    public FoodFindEvaluator(Ants ants) {
        this.ants = ants;
    }

    public Map<Ant, Probability> evaluate() {
        Map<Ant, Probability> evaluation = new LinkedHashMap<Ant, Probability>();

//        Map<Tile, Map<Ant, Way>> participants = new HashMap<Tile, Map<Ant, Way>>();

        // todo investigate on participants
        Map<Ant, List<Way>> involvedAnts = new LinkedHashMap<Ant, List<Way>>();
        for (Tile food : ants.getFoodTiles()) {
            Map<Ant, Way> distancesToFood = bfs(food);

            Ant nearestAnt = null;
            Way nearestWay = null;
            int minimumDistance = Integer.MAX_VALUE;
            for (Ant ant : distancesToFood.keySet()) {
                Way way = distancesToFood.get(ant);
                if (way.getDistance() < minimumDistance) {
                    minimumDistance = way.getDistance();
                    nearestWay = way;
                    nearestAnt = ant;
                }
            }

            if (nearestAnt != null) {
                List<Way> ways = involvedAnts.get(nearestAnt) != null ? involvedAnts.get(nearestAnt) : new ArrayList<Way>();
                ways.add(nearestWay);
                involvedAnts.put(nearestAnt, ways);
            }

//            Map<Ant, Way> nearestParticipants = new HashMap<Ant, Way>();
//            nearestParticipants.put(nearestAnt, nearestWay);
//
//            participants.put(food, nearestParticipants);
        }

        for (Ant ant : involvedAnts.keySet()) {
            int sum = 0;
            for (Way way : involvedAnts.get(ant))
                sum += way.getDistance();

            Map<Aim, Double> marks = new LinkedHashMap<Aim, Double>();
            double marksSum = 0.0;
            for (Way way : involvedAnts.get(ant)) {
                for (Aim direction : way.getDirections()) {
                    Double d = marks.get(direction) != null ? marks.get(direction) : 0.0;
                    d += sum / way.getDistance();
                    marks.put(direction, d);
                    marksSum += sum / way.getDistance();
                }
            }

            Probability antProbabilities = new Probability();
            for (Aim aim : marks.keySet()) {
                antProbabilities.put(aim, marks.get(aim) / marksSum);
            }
            antProbabilities.put(Aim.NONE, 0.0);
            evaluation.put(ant, antProbabilities);
        }

        return evaluation;
    }

    @Override
    public double getWeight() {
        return 1.2;
    }

    /**
     * Draft bfs implementation.
     *
     * @param startPoint starting position to find the ways
     * @return minimum distance and possible directions for each ant
     */
    public Map<Ant, Way> bfs(Tile startPoint) {
        int[][] nums = new int[ants.getRows()][ants.getCols()];
        for (int[] num : nums)
            Arrays.fill(num, -1);

        nums[startPoint.getRow()][startPoint.getCol()] = 0;
        Queue<Tile> ways = new ArrayDeque<Tile>();
        ways.add(startPoint);

        Map<Ant, Way> distances = new LinkedHashMap<Ant, Way>();
        Set<Tile> markedAnts = new LinkedHashSet<Tile>();

        int s = 1;
        while (!ways.isEmpty()) {
            Queue<Tile> newWays = new ArrayDeque<Tile>();
            for (Tile way : ways) {
                for (Aim aim : Aim.directions()) {
                    Tile nextDestination = ants.getTile(way, aim);

                    if (nums[nextDestination.getRow()][nextDestination.getCol()] == -1) {
                        if (ants.getIlk(nextDestination) == Ilk.MY_ANT)
                            markedAnts.add(nextDestination);

                        if (ants.getIlk(nextDestination).isPassable()) {
                            newWays.add(nextDestination);
                            nums[nextDestination.getRow()][nextDestination.getCol()] = s;
                        }
                    }
                }
            }

            s++;
            ways.clear();
            ways.addAll(newWays);
        }

        for (Tile markedAnt : markedAnts) {
            Set<Aim> directions = new LinkedHashSet<Aim>();
            for (Aim aim : Aim.directions()) {
                Tile t = ants.getTile(markedAnt, aim);
                if (nums[markedAnt.getRow()][markedAnt.getCol()] - nums[t.getRow()][t.getCol()] == 1)
                    directions.add(aim);
            }

            distances.put(ants.getAnt(markedAnt), new Way(nums[markedAnt.getRow()][markedAnt.getCol()], directions));
        }

        return distances;
    }
}
