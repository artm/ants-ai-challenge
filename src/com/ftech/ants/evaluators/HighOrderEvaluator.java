package com.ftech.ants.evaluators;

import com.ftech.ants.model.Ant;
import com.ftech.ants.model.Probability;

import java.util.Map;

public interface HighOrderEvaluator {
    public Map<Ant, Probability> evaluate(Map<Ant, Probability> ...evaluators);
}
