package com.ftech.ants.evaluators.LocalConflict;

import java.util.*;

import com.ftech.ants.Ants;
import com.ftech.ants.evaluators.Evaluator;
import com.ftech.ants.model.Aim;
import com.ftech.ants.model.Ant;
import com.ftech.ants.model.Probability;
import com.ftech.ants.model.Tile;

public class LocalConflictEvaluator implements Evaluator {
	private Ants ants;
	public static final double WEIGHT = 2;

	public LocalConflictEvaluator(Ants ants) {
		this.ants = ants;
		agroRadius = ants.getAttackRadius2() + 4 * (int) Math.sqrt(ants.getAttackRadius2()) + 4;
		attackRadius = ants.getAttackRadius2() + 2 * (int) Math.sqrt(ants.getAttackRadius2()) + 1;
	}

	private final Integer agroRadius;
	private final Integer attackRadius;

	boolean[] involvedEnemyAnts;
	boolean[] involvedMyAnts;
	Tile[] enemies;
	Tile[] friends;
	public static final int MY_ANT = 1;
	public static final int ENEMY_ANT = -1 * MY_ANT;
	public static final int CONFLICT = 8;
	int[][] advantageMap;
	int[][] conflicts;

	private void init() {
		involvedEnemyAnts = new boolean[ants.getEnemyAnts().size()];
		involvedMyAnts = new boolean[ants.getMyAnts().size()];
		enemies = new Tile[ants.getEnemyAnts().size()];
		int i = 0;
		for (Tile enemy : ants.getEnemyAnts()) {
			enemies[i++] = enemy;
		}
		i = 0;
		friends = new Tile[ants.getMyAnts().size()];
		for (Ant ant : ants.getMyAnts()) {
			friends[i++] = ant.getCurrentPosition();
		}
		advantageMap = new int[ants.getRows()][ants.getCols()];
		conflicts = new int[ants.getRows()][ants.getCols()];
	}

	@Override
	public Map<Ant, Probability> evaluate() {
        Map<Ant, Probability> evaluation = new LinkedHashMap<Ant, Probability>();

		init();
		for (int i = 0; i < involvedEnemyAnts.length; i++) {
			if (!involvedEnemyAnts[i]) {
				// DebugUtils.startTime();
				Situation conflictSituation = searchAllInvolvedAnts(i);
				makeDecision(conflictSituation, evaluation);
				// DebugUtils.endTime(i);
			}
		}

        return evaluation;
	}

    @Override
    public double getWeight() {
        return WEIGHT;
    }

    private final int MIN_ADVANATEGE = 0;

	private void makeDecision(Situation situation, Map<Ant, Probability> evaluation) {
		// DebugUtils.printEdgeCases(situation, ants);

		for (Tile myAnt : situation.myAnts) {
			fillMap(myAnt, MY_ANT);
		}
		// DebugUtils.printMap(advantageMap, conflicts, ants);
		for (Tile enemyAnt : situation.enemyAnts) {
			fillMap(enemyAnt, ENEMY_ANT);
		}
//		DebugUtils.printMap(advantageMap, conflicts, ants);
		for (Tile myAnt : situation.myAnts) {
			Ant ant = ants.getAnt(myAnt);

			Probability probabilities = new Probability();
			Set<Aim> directions = new HashSet<Aim>();
			for (Aim aim : Aim.directions()) {
				Tile nearest = ants.getTile(myAnt, aim);
				int maxAdvantage = Integer.MIN_VALUE;
				int advantage = advantageMap[nearest.getRow()][nearest.getCol()];
				Set<Aim> bestMoves = new HashSet<Aim>();
				while (advantage > 0) {
					int row = nearest.getRow();
					int col = nearest.getCol();
					if (conflicts[row][col] == CONFLICT && advantage > MIN_ADVANATEGE) {
						if (maxAdvantage == advantage) {
							bestMoves.add(aim);
						}
						if (advantage > maxAdvantage) {
							bestMoves = new HashSet<Aim>();
							bestMoves.add(aim);
							maxAdvantage = advantage;
						}
					}
					nearest = ants.getTile(nearest, aim);
					advantage = advantageMap[nearest.getRow()][nearest.getCol()];
				}
				directions.addAll(bestMoves);
			}

			if (directions.size() == 0) {
				directions = new HashSet<Aim>(Aim.directions());
			}
			double sumValue = 0;
			for (Aim aim : directions) {
				Tile nearest = ants.getTile(myAnt, aim);
				double wayValue = advantageMap[nearest.getRow()][nearest.getCol()] + 0.0;
				wayValue = Math.max(0, wayValue);
				probabilities.put(aim, wayValue);
				sumValue = sumValue + wayValue;
			}

			for (Aim aim : Aim.directions()) {
				Double probability = probabilities.get(aim);
				if (probability != null) {
					probabilities.put(aim, probability / sumValue);
				}
			}

			double moveWeight = WEIGHT;
			if (situation.myAnts.size() == 1) {
                // WTF?
				moveWeight = 1000;
			}

            evaluation.put(ant, probabilities);
//			ant.addProbabilities(probabilities, moveWeight);
		}
	}

	private void fillMap(Tile ant, int type) {
		Queue<Tile> ways = new ArrayDeque<Tile>();
		Set<Tile> visitedTiles = new HashSet<Tile>();
		visitedTiles.add(ant);
		ways.add(ant);

		while (!ways.isEmpty()) {
			Set<Tile> newWays = new HashSet<Tile>();
			for (Tile way : ways) {
				visitedTiles.add(way);
				checkConflicts(type, way);
				advantageMap[way.getRow()][way.getCol()] += type;
				// printMap(advantageMap);

				for (Aim aim : Aim.directions()) {
					Tile nextDestination = ants.getTile(way, aim);

					boolean canGo = ants.getIlk(nextDestination).isPassable() && !newWays.contains(nextDestination)
							&& !visitedTiles.contains(nextDestination);
					if (ants.getDistance(ant, nextDestination) <= attackRadius && canGo) {
						newWays.add(nextDestination);
					}
				}
			}
			ways.clear();
			ways.addAll(newWays);
		}
	}

	private void checkConflicts(int type, Tile way) {
		int row = way.getRow();
		int col = way.getCol();
		if (conflicts[row][col] != CONFLICT) {
			boolean isConflict = conflicts[row][col] + type == 0;
			if (isConflict) {
				conflicts[row][col] = CONFLICT;
			} else {
				conflicts[row][col] = type;
			}
		}
	}

	private Situation searchAllInvolvedAnts(int enemyNumber) {
		List<Tile> myAnts = new ArrayList<Tile>();
		// int lastProccesedFrienAnt = 0;
		// int lastProccesedEnemyAnt = 0;
		List<Tile> enemyAnts = new ArrayList<Tile>();
		enemyAnts.add(enemies[enemyNumber]);
		involvedEnemyAnts[enemyNumber] = true;
		boolean addNewAnt = false;
		do {
			addNewAnt = false;
			for (int i = 0; i < enemyAnts.size(); i++) {
				for (int j = 0; j < enemies.length; j++) {
					if (!involvedEnemyAnts[j] && ants.getDistance(enemies[j], enemyAnts.get(i)) <= agroRadius) {
						involvedEnemyAnts[j] = true;
						enemyAnts.add(enemies[j]);
						addNewAnt = true;
					}
				}
				for (int j = 0; j < friends.length; j++) {
					if (!involvedMyAnts[j] && ants.getDistance(friends[j], enemyAnts.get(i)) <= agroRadius) {
						involvedMyAnts[j] = true;
						myAnts.add(friends[j]);
						addNewAnt = true;
					}
				}
			}
			for (int i = 0; i < myAnts.size(); i++) {
				for (int j = 0; j < enemies.length; j++) {
					if (!involvedEnemyAnts[j] && ants.getDistance(enemies[j], myAnts.get(i)) <= agroRadius) {
						involvedEnemyAnts[j] = true;
						enemyAnts.add(enemies[j]);
						addNewAnt = true;
					}
				}
			}
		} while (addNewAnt);
		Situation situation = new Situation(enemyAnts, myAnts);
		return situation;
	}

	public class Situation {
		public List<Tile> myAnts;
		public List<Tile> enemyAnts;

		Situation(List<Tile> enemyAnts, List<Tile> myAnts) {
			this.myAnts = myAnts;
			this.enemyAnts = enemyAnts;
		}
	}
}
