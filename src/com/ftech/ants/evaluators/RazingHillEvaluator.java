package com.ftech.ants.evaluators;

import com.ftech.ants.Ants;
import com.ftech.ants.model.*;

import java.util.*;

public class RazingHillEvaluator implements Evaluator {
    private static final double WEIGHT = 2;

    private Ants ants;

    public RazingHillEvaluator(Ants ants) {
        this.ants = ants;
    }

    @Override
    public Map<Ant, Probability> evaluate() {
        Map<Ant, Probability> evaluation = new LinkedHashMap<Ant, Probability>();

        for (Tile enemyHill : ants.getVisibleEnemyHills()) {
            List<Tile> antsCloseToHill = new ArrayList<Tile>();
            for (Tile enemyAnt : ants.getEnemyAnts()) {
                if (ants.getDistance(enemyAnt, enemyHill) <= ants.getAttackRadius2())
                    antsCloseToHill.add(enemyAnt);
            }

            Map<Ant, Set<Aim>> closestAnts = findClosestAnts(enemyHill, antsCloseToHill.size() + 2);
            if (antsCloseToHill.size() == 0) {
                for (Ant ant : closestAnts.keySet()) {
                    Probability probabilities = new Probability();
                    for (Aim aim : closestAnts.get(ant)) {
                        probabilities.put(aim, 1.0 / closestAnts.get(ant).size());
                    }

                    evaluation.put(ant, probabilities);
                }
            } else {
                // todo handle situation when too much ants for attack involved

                boolean antWillMove = false;
                for (Ant ant : closestAnts.keySet()) {
                    Probability probabilities = new Probability();
                    for (Iterator<Aim> it = closestAnts.get(ant).iterator() ; it.hasNext() ; ) {
                        Aim aim = it.next();
                        Tile next = ants.getTile(ant.getCurrentPosition(), aim);

                        // if current step to my ant and we see an enemy - don't go there
                        boolean isEnemyVisible = false;
                        if (ants.getIlk(next) == Ilk.MY_ANT) {
                            for (Tile enemyAnt : antsCloseToHill) {
                                if (ants.getDistance(enemyAnt, ant.getCurrentPosition()) <= ants.getViewRadius2()) {
                                    isEnemyVisible = true;
                                    it.remove();
                                    break;
                                }
                            }
                        }

                        if (!isEnemyVisible) {
                            for (Tile enemyAnt : antsCloseToHill) {
                                if (ants.getDistance(enemyAnt, next) <= ants.getAttackRadius2()) {
                                    it.remove();
                                    break;
                                }
                            }
                        }

                    }

                    for (Aim aim : closestAnts.get(ant)) {
                        antWillMove = true;
                        probabilities.put(aim, 1.0 / closestAnts.get(ant).size());
                    }

                    if (antWillMove) {
                        evaluation.put(ant, probabilities);
                    }
                }

                if (!antWillMove && closestAnts.size() == antsCloseToHill.size() + 2) {
                    // Attack!
                    for (Ant ant : closestAnts.keySet()) {
                        Probability probabilities = new Probability();
                        for (Aim aim : closestAnts.get(ant)) {
                            probabilities.put(aim, 1.0 / closestAnts.get(ant).size());
                        }

                        evaluation.put(ant, probabilities);
                    }
                }
            }
        }

        return evaluation;
    }

    @Override
    public double getWeight() {
        return WEIGHT;
    }

    private Map<Ant, Set<Aim>> findClosestAnts(Tile startPoint, int antCount) {
        int[][] nums = new int[ants.getRows()][ants.getCols()];
        for (int[] num : nums)
            Arrays.fill(num, -1);

        nums[startPoint.getRow()][startPoint.getCol()] = 0;
        Queue<Tile> ways = new ArrayDeque<Tile>();
        ways.add(startPoint);

        Map<Ant, Set<Aim>> closestAnts = new LinkedHashMap<Ant, Set<Aim>>();

        int s = 1;
        while (!ways.isEmpty() && closestAnts.size() < antCount) {
            Queue<Tile> newWays = new ArrayDeque<Tile>();
            for (Tile way : ways) {
                for (Aim aim : Aim.directions()) {
                    Tile nextDestination = ants.getTile(way, aim);

                    if (nums[nextDestination.getRow()][nextDestination.getCol()] == -1) {
                        if (ants.getIlk(nextDestination) == Ilk.MY_ANT) {
                            if (closestAnts.size() < antCount)
                                closestAnts.put(ants.getAnt(nextDestination), new LinkedHashSet<Aim>());
                        }

                        if (ants.getIlk(nextDestination).isPassable()) {
                            newWays.add(nextDestination);
                            nums[nextDestination.getRow()][nextDestination.getCol()] = s;
                        }
                    }
                }
            }

            s++;
            ways.clear();
            ways.addAll(newWays);
        }

        for (Ant ant : closestAnts.keySet()) {
            for (Aim aim : Aim.directions()) {
                Tile next = ants.getTile(ant.getCurrentPosition(), aim);
                if (nums[next.getRow()][next.getCol()] + 1 == nums[ant.getCurrentPosition().getRow()][ant.getCurrentPosition().getCol()]) {
                    closestAnts.get(ant).add(aim);
                }
            }
        }

        return closestAnts;
    }

}
