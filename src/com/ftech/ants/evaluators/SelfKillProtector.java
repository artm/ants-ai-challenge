package com.ftech.ants.evaluators;

import com.ftech.ants.Ants;
import com.ftech.ants.evaluators.LocalConflict.DebugUtils;
import com.ftech.ants.model.*;

import java.util.*;

public class SelfKillProtector implements HighOrderEvaluator {
	private Ants ants;

	public SelfKillProtector(Ants ants) {
		this.ants = ants;
	}

	@Override
	public Map<Ant, Probability> evaluate(Map<Ant, Probability> ...evaluators) {
        assert evaluators.length == 1;

        Map<Ant, Probability> evaluation = evaluators[0];

		boolean selfKilling;
		Map<Tile, List<Ant>> previousExpectedMoves = null;
		do {
			// I. Removing impossible moves
			// DebugUtils.printMessage("I. Removing impossible moves");
            for (Ant ant : evaluation.keySet()) {
                Aim direction = ant.getMaxProbabilityTurn(ants.getRandom()).getKey();
                if (ants.getIlk(ant.getCurrentPosition(), direction) == Ilk.WATER) {
                    evaluation.get(ant).removeProbability(direction);
                }
            }

			// II. Gather collisions
			// DebugUtils.printMessage("II. Gather collisions");
			Map<Tile, List<Ant>> expectedMoves = new LinkedHashMap<Tile, List<Ant>>();
			for (Ant ant : evaluation.keySet()) {
				Tile expected;

				Map.Entry<Aim, Double> maxProbabilityTurn = ant.getMaxProbabilityTurn(ants.getRandom());
				expected = ants.getTile(ant.getCurrentPosition(), maxProbabilityTurn.getKey());

				if (ants.getIlk(expected) == Ilk.WATER || ants.getIlk(expected) == Ilk.FOOD) {
					expected = ant.getCurrentPosition();
                }

				if (expectedMoves.get(expected) == null)
					expectedMoves.put(expected, new ArrayList<Ant>());

				expectedMoves.get(expected).add(ant);
			}

			// III. Resolve collision
			// DebugUtils.printMessage("III. Resolve collision");
			selfKilling = false;
			for (Tile tile : expectedMoves.keySet()) {
				if (expectedMoves.get(tile).size() > 1) {
					selfKilling = true;

					double maxProbability = 0.0;
					Ant maximumProbable = null;
					for (Ant ant : expectedMoves.get(tile)) {
						Map.Entry<Aim, Double> maxProbabilityTurn = ant.getMaxProbabilityTurn(ants.getRandom());
						if (maxProbability < maxProbabilityTurn.getValue()) {
							maximumProbable = ant;
							maxProbability = maxProbabilityTurn.getValue();
						}
					}

					for (Ant ant : expectedMoves.get(tile)) {
						if (ant != maximumProbable) {
							evaluation.get(ant).removeProbability(ant.getMaxProbabilityTurn(ants.getRandom()).getKey());
						}
					}
				}
			}
			if (expectedMoves.equals(previousExpectedMoves)) {
				// todo Fix timeout with infinitive loops 
				return evaluation;
			}
			previousExpectedMoves = expectedMoves;
		} while (selfKilling);

        return evaluation;
	}
}
