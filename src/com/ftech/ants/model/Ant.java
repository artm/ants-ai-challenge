package com.ftech.ants.model;

import java.util.*;

public class Ant {
    private Probability turnProbability = new Probability();
    private Tile currentPosition;

    private boolean probabilityChanged = true;
    private Map.Entry<Aim, Double> lastComputedProbability = new AbstractMap.SimpleEntry<Aim, Double>(Aim.NONE, 1.0 / Aim.values().length);

    public Ant(Tile currentPosition) {
        this.currentPosition = currentPosition;
        turnProbability.fillDefault();
    }

    public Tile getCurrentPosition() {
        return currentPosition;
    }

    @Override
    public int hashCode() {
        return currentPosition.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof Ant) {
            Ant ant = (Ant) obj;
            result = currentPosition.equals(ant.currentPosition);
        }
        return result;
    }

    
    @Override
    public String toString() {
    	return currentPosition.toString();
    }
    public Map.Entry<Aim, Double> getMaxProbabilityTurn(Random random) {
        if (!probabilityChanged) return lastComputedProbability;

        List<Aim> equalProbabilityAims = new ArrayList<Aim>();
        double maxProbability = 0.0;

        for (Aim aim : turnProbability.keySet()) {
            if (turnProbability.get(aim) > maxProbability) {
                maxProbability = turnProbability.get(aim);
                equalProbabilityAims.clear();
                equalProbabilityAims.add(aim);
            } else if (turnProbability.get(aim) == maxProbability)
                equalProbabilityAims.add(aim);
        }

        Aim maxProbable = equalProbabilityAims.get(Math.abs(random.nextInt()) % equalProbabilityAims.size());
        lastComputedProbability = new AbstractMap.SimpleEntry<Aim, Double>(maxProbable, turnProbability.get(maxProbable));
        probabilityChanged = false;

        return lastComputedProbability;
    }

    public void addProbabilities(Probability probabilities, double weight) {
        turnProbability.addProbability(probabilities, weight);
    }

    public void setAntProbability(Probability turnProbability) {
        this.turnProbability = turnProbability;
        probabilityChanged = true;
    }

    public void removeProbability(Aim aim) {
        turnProbability.removeProbability(aim);

        probabilityChanged = true;
    }
}
