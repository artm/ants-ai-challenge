package com.ftech.ants.model;

/**
 * Represents type of tile on the game map.
 */
public enum Ilk {
    /** Unexplored tile */
    UNEXPLORED,

    /** Water tile. */
    WATER,
    
    /** Food tile. */
    FOOD,
    
    /** Land tile. */
    LAND,
    
    /** My dead ant tile. */
    MY_DEAD,
    
    /** Enemy dead ant tile. */
    ENEMY_DEAD,

    /** My ant tile. */
    MY_ANT,
    
    /** Enemy ant tile. */
    ENEMY_ANT;
    
    /**
     * Checks if this type of tile is passable, which means it is not a water tile.
     * 
     * @return <code>true</code> if this is not a water tile, <code>false</code> otherwise
     */
    public boolean isPassable() {
        return ordinal() > WATER.ordinal();
    }
    
    /**
     * Checks if this type of tile is unoccupied, which means it is a land tile or a dead ant tile.
     * 
     * @return <code>true</code> if this is a land tile or a dead ant tile, <code>false</code>
     *         otherwise
     */
    public boolean isUnoccupied() {
        return this == LAND || this == MY_DEAD || this == ENEMY_DEAD;
    }
}
