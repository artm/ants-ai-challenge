package com.ftech.ants.model;

import com.ftech.ants.AntsParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Movement {
    private AntsParser.Action action;
    private Map<Enum, List<Object>> info = new HashMap<Enum, List<Object>>();

    public AntsParser.Action getAction() {
        return action;
    }

    public void setAction(AntsParser.Action action) {
        this.action = action;
    }

    public Object put(Enum key, Object value) {
        if (!info.containsKey(key)) info.put(key, new ArrayList<Object>());
        return info.get(key).add(value);
    }

    public List<Object> get(Enum key) {
        return info.get(key) != null ? info.get(key) : new ArrayList<Object>();
    }
}
