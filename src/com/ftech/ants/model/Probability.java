package com.ftech.ants.model;

import java.util.*;

public class Probability extends LinkedHashMap<Aim, Double> {
    public Probability() {
        for (Aim aim : Aim.values()) {
            put(aim, 0.0);
        }
    }

    public void fillDefault() {
        for (Aim aim : Aim.values()) {
            put(aim, 1.0 / Aim.values().length);
        }
    }

    public void addProbability(Probability probability, double weight) {
        for (Aim aim : Aim.values()) {
            Double previousProbability = get(aim) == null ? 0.0 : get(aim);
            Double addedProbability = probability.get(aim) == null ? 0.0 : probability.get(aim);
            put(aim, (previousProbability + addedProbability * weight) / (1 + weight));
        }
    }

    public void removeProbability(Aim aim) {
        List<Aim> aims = new ArrayList<Aim>();
        aims.addAll(Arrays.asList(Aim.values()));
        aims.remove(aim);

        double loosenProbability = get(aim);
        put(aim, 0.0);
        for (Aim increasedAim : aims) {
            put(increasedAim, get(increasedAim) + loosenProbability / 4);
        }
    }
}
