package com.ftech.ants.model;

import java.util.Set;

public class Way {
    private Integer distance;
    private Set<Aim> directions;

    public Way(Integer distance, Set<Aim> directions) {
        this.distance = distance;
        this.directions = directions;
    }

    public Integer getDistance() {
        return distance;
    }

    public Set<Aim> getDirections() {
        return directions;
    }
}
