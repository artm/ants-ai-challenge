turn 0
loadtime 3000
turntime 1000
rows 43
cols 39
turns 1000
viewradius2 55
attackradius2 5
spawnradius2 1
player_seed 0
ready

turn 1
h 28 19 0
a 14 29 0
a 20 24 0
a 21 18 1
a 21 20 1
a 21 23 0
a 22 17 1
a 26 24 0
a 27 19 0
a 28 16 0
a 28 17 0
a 28 20 0
a 28 29 0
a 29 19 0
a 29 30 0
a 30 20 0
a 31 19 0
a 31 26 0
a 32 7 0
a 34 12 0
a 34 25 0
a 35 24 0
a 40 24 0
f 8 26
f 14 25
f 28 13
go
end