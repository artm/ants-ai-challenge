package com.ftech.ants;

import java.util.Arrays;

public class AttackModeling {
    static int[][] directions = {{0, 0}, {0, 1}, {1, 0}, {-1, 0}, {0, -1}};

    static int[][] a = new int[20][20];

    /*
    * 0 1 1 1 0
    * 1 1 1 1 1
    * 1 1 1 1 1
    * 1 1 1 1 1
    * 0 1 1 1 0
    * */
    public static void potential(int row, int column, int sign) {
        for (int i = row - 2 ; i <= row + 2 ; i++)
            for (int j = column - 2 ; j <= column + 2 ; j++) {
                if ((i - row) * (i - row) + (j - column) * (j - column) <= 5) {
                    a[i][j] += sign;
                }
            }
    }

    public static void main(String[] args) {
        int startPoint = 10;
        for (int[] direction : directions) {
            potential(startPoint + direction[0], startPoint + direction[1], 1);
        }

        for (int[] direction : directions) {
            potential(12 + direction[0], 8 + direction[1], -1);
        }

        for (int[] direction : directions) {
            potential(12 + direction[0], 12 + direction[1], -1);
        }


        for (int[] ints : a) {
            for (int i : ints) {
                System.out.printf("%6.2f", i / 5.0);
            }
            System.out.println();
        }
    }
}
