package com.ftech.ants.test.data.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.ftech.ants.Ants;
import com.ftech.ants.model.Ilk;
import com.ftech.ants.model.Tile;

public class MapParser {

	public Ants parseMap(InputStream inputStream) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String line = reader.readLine();
		Integer rows = Integer.parseInt(line.split(" ")[1]);
		line = reader.readLine();
		Integer col = Integer.parseInt(line.split(" ")[1]);

		Ants map = null;/*new Ants(1000, 1000, rows, col, 1000, 55, 5, 1, 42);*/

		int i = 0;
		while ((line = reader.readLine()) != null) {
			int j = 0;
			for (char entry : line.toCharArray()) {
				Tile tile = new Tile(i, j);
				if (entry == '%') {
					map.update(Ilk.WATER, tile);
				} else if (entry == 'a') {
					map.update(Ilk.MY_ANT, tile);
				} else if (entry == '.') {
					map.update(Ilk.LAND, tile);
				} else if (entry == '*') {
					map.update(Ilk.FOOD, tile);
				}
				j++;
			}
			i++;
		}

		return map;
	}
}
